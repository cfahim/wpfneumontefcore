﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfNeumontClass.ViewModels;

namespace WpfNeumontClass
{
    /// <summary>
    /// Interaction logic for StudentDetailWindow.xaml
    /// </summary>
    public partial class StudentDetailWindow : Window
    {
        public readonly StudentViewModel studentViewModel;

        public StudentDetailWindow()
        {
            InitializeComponent();
            studentViewModel =
                (StudentViewModel)FindResource(nameof(StudentViewModel));

        }

    }
}
