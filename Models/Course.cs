﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace WpfNeumontClass.Models
{
    public class Course
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public float Credits { get; set; }

        public ICollection<Student> Students { get; set; } =
            new ObservableCollection<Student>();
    }
}
