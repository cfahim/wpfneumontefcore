﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace WpfNeumontClass.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public float GPA { get; set; }

        public ICollection<Course> Courses { get; set; } =
            new ObservableCollection<Course>();
    }
}
