﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WpfNeumontClass.Models;

namespace WpfNeumontClass.Data
{
    public class NeumontContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }

        private readonly string host = "localhost";
        private readonly string db = "NeumontClass";
        private readonly string username = "neumontuser";
        private readonly string password = "neumontpassword";

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql($"Host={host};Database={db};Username={username};Password={password};");
        
    }
}
