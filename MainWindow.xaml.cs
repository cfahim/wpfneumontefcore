﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using WpfNeumontClass.Data;
using WpfNeumontClass.Models;

namespace WpfNeumontClass
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly NeumontContext _context =
            new NeumontContext();

        private CollectionViewSource studentsViewSource;

        public MainWindow()
        {
            InitializeComponent();
            studentsViewSource =
                (CollectionViewSource)FindResource(nameof(studentsViewSource));
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _context.Database.Migrate();

            _context.Students
                .Include(s => s.Courses)
                .Load();

            studentsViewSource.Source =
                _context.Students.Local.ToObservableCollection();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            _context.Dispose();
            base.OnClosing(e);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _context.SaveChanges();
            studentDataGrid.Items.Refresh();
            studentCourseDataGrid.Items.Refresh();
        }

        private void StudentDataGridDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var row = sender as DataGridRow;
            var student = row.Item as Student;
            var sdw = new StudentDetailWindow();
            sdw.studentViewModel.Student = student;
            sdw.studentViewModel.Context = _context;
            sdw.studentViewModel.StudentEdited += StudentEdited;
            sdw.Show();
        }

        private void StudentEdited(object sender, Student e)
        {
            studentDataGrid.Items.Refresh();
            studentCourseDataGrid.Items.Refresh();
        }
    }
}
