﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using WpfNeumontClass.Data;
using WpfNeumontClass.Models;

namespace WpfNeumontClass.ViewModels
{
    public class StudentViewModel : ViewModelBase
    {
        public NeumontContext Context { get; set; }

        public event EventHandler<Student> StudentEdited; 

        private Student _student;
        public Student Student
        {
            get
            {
                return _student;
            }
            set
            {
                _student = value;
                NotifyPropertyChanged(nameof(Student));
            }
        }


        public ICommand _EditSubmitCommand;
        public ICommand EditSubmitCommand
        {
            get
            {
                if(_EditSubmitCommand == null)
                {
                    _EditSubmitCommand = new RelayCommand(param => EditSubmit(),
                        null);
                }
                return _EditSubmitCommand;
            }
        }

        private void EditSubmit()
        {
            try
            {
                Context.SaveChanges();
                StudentEdited?.Invoke(this, Student);
            }
            catch(Exception)
            {
                //do something on db error
            }
        }

    }
}
